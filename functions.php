<?php

function custom_get_image_path () {
	return '/../apps/blogs/wp-content' . get_image_path();
}

function custom_excerpt_more( $more ) {
	return '&#8230;';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

?>