<?php get_header(); ?>

<!-- Begin #colleft -->
			<div id="colLeft">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
				<div id="singlePost">
					<h1><?php the_title(); ?></h1>
					<div class="meta"> <?php the_time('F Y') ?> <?php if ('open' == $post->comment_status) : ?>&nbsp;&nbsp;&nbsp;<img src="<?php bloginfo('template_directory'); ?>/images/ico_post_comments.png" alt="" /> <?php comments_popup_link('No Comments', '1 Comment ', '% Comments'); ?><?php else : // comments are closed ?><?php endif; ?>
					</div>

					<?php the_content(); ?>
					 <div class="postTags"><?php the_tags(); ?></div>

				</div>
								
		<?php endwhile; else: ?>

		<p>Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>
			
			</div>
			<!-- End #colLeft -->
			

<?php get_sidebar(); ?>	
<?php get_footer(); ?>